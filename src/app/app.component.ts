import { Component, Inject, OnInit } from '@angular/core';
import { NotesService } from './notes.service';
import { Note } from './note';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  notes: Note[] = [];
  selected: Note;
  errorMessage = '';

  constructor(private service: NotesService) {

  }

  ngOnInit() {
    this.loadNotes();
  }

  getNotes() {
    return this.notes;
  }

  private loadNotes(): void {
    this.notes = this.service.getNotes();
  }

  selectNote(note) {
    this.selected = {...note};
  }

  newNote() {
    this.selected = {
      id: 0,
      title: '',
      body: '',
      color: '#ff0000',
      favourite: false
    }
  }

  saveNote(note: Note) {
    note = {...note};
    note.title = note.title.trim();

    // Title can't be empty
    if (note.title) {
      this.service.saveNote(this.selected);
    } else {
      this.errorMessage = 'Please enter a Title.';
    }
  }
}
